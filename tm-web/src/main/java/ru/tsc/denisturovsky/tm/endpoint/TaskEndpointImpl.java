package ru.tsc.denisturovsky.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.denisturovsky.tm.api.service.dto.ITaskDTOService;
import ru.tsc.denisturovsky.tm.client.TaskRestEndpointClient;
import ru.tsc.denisturovsky.tm.dto.model.TaskDTO;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskEndpointImpl implements TaskRestEndpointClient {

    @Autowired
    private ITaskDTOService taskService;

    @NotNull
    @Override
    @PostMapping("/add")
    public TaskDTO add(@RequestBody @NotNull final TaskDTO task) throws Exception {
        return taskService.add(task);
    }

    @Override
    @PostMapping("/deleteAll")
    public void clear() throws Exception {
        taskService.clear();
    }

    @Override
    @GetMapping("/count")
    public long count() throws Exception {
        return taskService.count();
    }

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody @NotNull final TaskDTO task) throws Exception {
        taskService.remove(task);
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") @NotNull final String id) throws Exception {
        taskService.removeById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@PathVariable("id") @NotNull final String id) throws Exception {
        return (taskService.findOneById(id) != null);
    }

    @Override
    @GetMapping("/findAll")
    public List<TaskDTO> findAll() throws Exception {
        return taskService.findAll();
    }

    @Nullable
    @Override
    @GetMapping("/findAllByProjectId/{projectId}")
    public List<TaskDTO> findAllByProjectId(@PathVariable("projectId") @NotNull final String projectId) throws Exception {
        return taskService.findAllByProjectId(projectId);
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    public TaskDTO findById(@PathVariable("id") @NotNull final String id) throws Exception {
        return taskService.findOneById(id);
    }

    @NotNull
    @Override
    @PostMapping("/save")
    public TaskDTO save(@RequestBody @NotNull final TaskDTO task) throws Exception {
        return taskService.update(task);
    }

}