package ru.tsc.denisturovsky.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.denisturovsky.tm.api.service.dto.IProjectDTOService;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.denisturovsky.tm.exception.field.DescriptionEmptyException;
import ru.tsc.denisturovsky.tm.exception.field.IdEmptyException;
import ru.tsc.denisturovsky.tm.exception.field.NameEmptyException;
import ru.tsc.denisturovsky.tm.exception.field.StatusEmptyException;
import ru.tsc.denisturovsky.tm.repository.ProjectDTORepository;

import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class ProjectDTOService implements IProjectDTOService {

    @Nullable
    @Autowired
    private ProjectDTORepository repository;

    @NotNull
    @Override
    @Transactional
    public ProjectDTO add(@NotNull final ProjectDTO model) throws Exception {
        return repository.save(model);
    }

    @Override
    @Transactional
    public void changeProjectStatusById(
            @Nullable final String id,
            @Nullable final Status status
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final ProjectDTO project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        repository.save(project);
    }

    @Override
    @Transactional
    public void clear() throws Exception {
        repository.deleteAll();
    }

    @Override
    public int count() throws Exception {
        return (int) repository.count();
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO create(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(name);
        return repository.save(project);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO create(
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        return repository.save(project);
    }

    @Override
    public boolean existsById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll() throws Exception {
        return repository.findAll();
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Optional<ProjectDTO> result = repository.findById(id);
        return result.orElse(null);
    }

    @Override
    @Transactional
    public void remove(@Nullable final ProjectDTO model) throws Exception {
        if (model == null) return;
        repository.delete(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public ProjectDTO update(@Nullable final ProjectDTO model) throws Exception {
        if (model == null) return null;
        return repository.save(model);
    }

    @Override
    @Transactional
    public void updateById(
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final ProjectDTO project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
    }

}