package ru.tsc.denisturovsky.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.denisturovsky.tm.api.service.dto.IProjectDTOService;
import ru.tsc.denisturovsky.tm.client.ProjectRestEndpointClient;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectEndpointImpl implements ProjectRestEndpointClient {

    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Override
    @PostMapping("/add")
    public ProjectDTO add(@RequestBody final @NotNull ProjectDTO project) throws Exception {
        return projectService.add(project);
    }

    @Override
    @PostMapping("/deleteAll")
    public void clear() throws Exception {
        projectService.clear();
    }

    @Override
    @GetMapping("/count")
    public long count() throws Exception {
        return projectService.count();
    }

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody @NotNull final ProjectDTO project) throws Exception {
        projectService.remove(project);
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") @NotNull final String id) throws Exception {
        projectService.removeById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@PathVariable("id") @NotNull final String id) throws Exception {
        ProjectDTO p = projectService.findOneById(id);
        return (projectService.findOneById(id) != null);
    }

    @Override
    @GetMapping("/findAll")
    public List<ProjectDTO> findAll() throws Exception {
        return projectService.findAll();
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    public ProjectDTO findById(@PathVariable("id") @NotNull final String id) throws Exception {
        return projectService.findOneById(id);
    }

    @NotNull
    @Override
    @PostMapping("/save")
    public ProjectDTO save(@RequestBody @NotNull final ProjectDTO project) throws Exception {
        return projectService.update(project);
    }

}