package ru.tsc.denisturovsky.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.denisturovsky.tm.client.ProjectRestEndpointClient;
import ru.tsc.denisturovsky.tm.client.TaskRestEndpointClient;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;
import ru.tsc.denisturovsky.tm.dto.model.TaskDTO;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.marker.IntegrationCategory;

import java.util.List;

import static ru.tsc.denisturovsky.tm.constant.ProjectTestData.USER_PROJECT1_DESCRIPTION;
import static ru.tsc.denisturovsky.tm.constant.ProjectTestData.USER_PROJECT1_NAME;
import static ru.tsc.denisturovsky.tm.constant.TaskTestData.*;

@Category(IntegrationCategory.class)
public final class TaskRestEndpointTest {

    @NotNull
    private final TaskRestEndpointClient taskEndpointClient = TaskRestEndpointClient.client();

    @NotNull
    private final ProjectRestEndpointClient projectEndpointClient = ProjectRestEndpointClient.client();

    @NotNull
    private ProjectDTO project1 = new ProjectDTO(USER_PROJECT1_NAME, USER_PROJECT1_DESCRIPTION);

    @NotNull
    private TaskDTO task1 = new TaskDTO(USER_TASK1_NAME, USER_TASK1_DESCRIPTION);

    @NotNull
    private TaskDTO task2 = new TaskDTO(USER_TASK2_NAME, USER_TASK2_DESCRIPTION);

    @NotNull
    private TaskDTO task3 = new TaskDTO(USER_TASK3_NAME, USER_TASK3_DESCRIPTION);

    private long baseCount = 0;

    @After
    public void after() throws Exception {
        taskEndpointClient.delete(task1);
        taskEndpointClient.delete(task2);
        taskEndpointClient.delete(task3);
        projectEndpointClient.delete(project1);
    }

    @Before
    public void before() throws Exception {
        baseCount = taskEndpointClient.findAll().size();
        projectEndpointClient.add(project1);
        task1.setProjectId(project1.getId());
        taskEndpointClient.add(task1);
        taskEndpointClient.add(task2);
    }

    @Test
    public void testAdd() throws Exception {
        @Nullable TaskDTO task = taskEndpointClient.add(task3);
        Assert.assertNotNull(task);
        Assert.assertEquals(task3.getName(), task.getName());
        Assert.assertEquals(task3.getDescription(), task.getDescription());
    }

    @Test
    public void testCount() throws Exception {
        Assert.assertEquals(baseCount + 2, taskEndpointClient.count());
    }

    @Test
    public void testDelete() throws Exception {
        taskEndpointClient.delete(task1);
        Assert.assertNull(taskEndpointClient.findById(task1.getId()));
    }

    @Test
    public void testDeleteById() throws Exception {
        taskEndpointClient.deleteById(task1.getId());
        Assert.assertNull(taskEndpointClient.findById(task1.getId()));
    }

    @Test
    public void testExistsById() throws Exception {
        Assert.assertTrue(taskEndpointClient.existsById(task1.getId()));
        Assert.assertFalse(taskEndpointClient.existsById(task3.getId()));
    }

    @Test
    public void testFindAll() throws Exception {
        @Nullable final List<TaskDTO> tasks = taskEndpointClient.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(baseCount + 2, tasks.size());
        for (@NotNull TaskDTO task : tasks) {
            Assert.assertNotNull(taskEndpointClient.findById(task.getId()));
        }
    }

    @Test
    public void testFindAllByProjectId() throws Exception {
        @Nullable final List<TaskDTO> tasks = taskEndpointClient.findAllByProjectId(task1.getProjectId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        for (@NotNull TaskDTO task : tasks) {
            Assert.assertNotNull(taskEndpointClient.findById(task.getId()));
        }
    }

    @Test
    public void testFindById() throws Exception {
        @Nullable TaskDTO task = taskEndpointClient.findById(task1.getId());
        Assert.assertEquals(USER_TASK1_NAME, task.getName());
        Assert.assertEquals(USER_TASK1_DESCRIPTION, task.getDescription());
    }

    @Test
    public void testSave() throws Exception {
        @Nullable TaskDTO task = taskEndpointClient.findById(task1.getId());
        task.setStatus(Status.IN_PROGRESS);
        Assert.assertNotNull(taskEndpointClient.save(task));
        @Nullable TaskDTO task2 = taskEndpointClient.findById(task1.getId());
        Assert.assertEquals(task.getStatus(), task2.getStatus());
    }

}